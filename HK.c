//	------- Housekeeping ADC sampling routines -----------

#ifndef STM32F0 // helps Xcode resolve headers
#define STM32F0
#endif

#include <libopencm3/stm32/gpio.h>
#include <libopencm3/stm32/timer.h>
#include <libopencm3/stm32/adc.h>
#include <libopencm3/cm3/nvic.h>

#include "HK.h"

// Set up for single chan software trig conversion.
// Assumes GPIO bits are already set up
void hk_setup(void)
{
    adc_power_off(ADC1);
    adc_calibrate(ADC1); // requires adc disabled
    while (adc_is_calibrating(ADC1))
        ;

    adc_set_sample_time_on_all_channels(ADC1, ADC_SMPR_SMP_055DOT5); // works best for SWPMON
    adc_power_on(ADC1);

    adc_enable_temperature_sensor();
    adc_enable_vrefint();

    adc_set_clk_source(ADC1, ADC_CLKSOURCE_PCLK_DIV2); // use sync clock for no jitter
    adc_set_resolution(ADC1, ADC_CFGR1_RES_12_BIT);
    adc_set_right_aligned(ADC1);
    adc_disable_external_trigger_regular(ADC1);
    adc_set_single_conversion_mode(ADC1);
}