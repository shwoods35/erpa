/**
 * @file spi_eadc.c
 * @author Shane Wooods and Jared King
 * @brief Code to read voltage from a 16-bit external ADC (AD7980)
 * @version 0.1
 * @date 2022-06-13
 *
 * IC pin Configuration for AD7980
 *
 *               AD7980 +--------+ AD7980
 *  +5v   <----> Vref 1 |        | 10 VIO <---> +3.3v
 *  +2.5v <----> Vdd  2 |        | 9  SDI <---> VIO
 *  +2.5v <----> IN+  3 |        | 8  SCK <---> STM32 PA5 (CLOCK)
 *  GND   <----> IN-  4 |        | 7  SDO <---> STM32 PA6 (MOSI)
 *  GND   <----> GND  5 |        | 6  CNV <---> STM32 PA8 (TIM1)
 *                      +--------+
 */

#ifndef STM32F0
#define STM32F0
#endif

#include <libopencm3/stm32/gpio.h>
#include <libopencm3/stm32/timer.h>
#include <libopencm3/cm3/nvic.h>
#include <libopencm3/stm32/spi.h>
#include <libopencm3/stm32/adc.h>

#include "uart.h"
#include "samp.h"
#include "HK.h"

#include <stdio.h>
#include <string.h>

void tim1_cc_isr(void)
{

  /* Toggle Blue LED on interrupt */
  gpio_toggle(LED_PORT, GREEN_LED_PIN);

  /* Send byte to initialize SPI transfer */
  SPI1_DR = 0x1;
  while (!(SPI1_SR & SPI_SR_RXNE))
    ; /* wait for SPI transfer complete */

  /* Get raw adc value from data register */
  int raw = SPI1_DR;

  uint8_t MSB = ((raw & 0xFF00) >> 8); /* MSB HERE */
  uint8_t LSB = (raw & 0xFF);          /* LSB HERE */
  raw = 0x0;
  raw = (raw | LSB);
  raw = raw << 8;
  raw = raw | MSB;

  printRaw(raw);

  /* Send RPA sample (SPI readout has bytes swapped) */
  // putswap(raw);

  /**
   * Equation to calculate voltage
   * Voltage = Raw * (5 / 65535)
   */
  /* extern ADC readout completed. Force CNV low */
  TIM1_CCMR1 = TIM_CCMR1_OC1M_FORCE_HIGH; // (assumes all other bits are zero)
  TIM1_CCMR1 = TIM_CCMR1_OC1M_TOGGLE;

  TIM1_SR = ~TIM_SR_CC1IF; // clear interrupt
}

void spi1_setup(void)
{
  /* Reset SPI, SPI_CR1 register cleared, SPI is disabled */
  spi_reset(SPI1);

  /* Set up SPI in Master mode with:
   * Clock baud rate: 1/4 of peripheral clock frequency
   * Clock polarity: Idle High
   * Clock phase: Data valid on 1st clock pulse
   * Data frame format: 8-bit
   * Frame format: MSB First
   */
  spi_init_master(SPI1, SPI_BUADRATE_PRESCALER, SPI_CLOCK_POLARITY,
                  SPI_CPHA_CLOCK_TRANSITION, SPI_MSB_FIRST);

  /* AD7980 is 16-bit resolution, need to set data size to 16-bits */
  spi_set_data_size(SPI1, 16);

  /*
   * Set NSS management to software.
   *
   * Note:
   * Setting nss high is very important, even if we are controlling the GPIO
   * ourselves this bit needs to be at lea st set to 1, otherwise the spi
   * peripheral will not send any data out.
   */
  spi_enable_software_slave_management(SPI1);
  spi_set_nss_high(SPI1);
  /* Enable SPI1 periph. */
  spi_enable(SPI1);
}

void spi2_setup(void)
{
  /* Reset SPI, SPI_CR1 register cleared, SPI is disabled */
  spi_reset(SPI2);

  /* Set up SPI in Master mode with:
   * Clock baud rate: 1/4 of peripheral clock frequency
   * Clock polarity: Idle High
   * Clock phase: Data valid on 1st clock pulse
   * Data frame format: 8-bit
   * Frame format: MSB First
   */
  spi_init_master(SPI2, SPI_BUADRATE_PRESCALER, SPI_CLOCK_POLARITY,
                  SPI_CPHA_CLOCK_TRANSITION, SPI_MSB_FIRST);

  /* AD7980 is 16-bit resolution, need to set data size to 16-bits */
  spi_set_data_size(SPI2, 16);

  /*
   * Set NSS management to software.
   *
   * Note:
   * Setting nss high is very important, even if we are controlling the GPIO
   * ourselves this bit needs to be at lea st set to 1, otherwise the spi
   * peripheral will not send any data out.
   */
  spi_enable_software_slave_management(SPI2);
  spi_set_nss_high(SPI2);
  /* Enable SPI1 periph. */
  spi_enable(SPI2);
}

void timer1_setup(void)
{

  /* Disable/Reset Timer? */
  timer_set_mode(TIM1, TIM_CR1_CKD_CK_INT, TIM_CR1_CMS_EDGE, TIM_CR1_DIR_UP); // FIXME THIS IS NEW

  /* Enables TIM1_CH1 in Output Compare Mode */
  timer_set_oc_mode(TIM1, TIM_OC1, TIM_OCM_TOGGLE);
  timer_enable_oc_output(TIM1, TIM_OC1);
  timer_enable_break_main_output(TIM1);

  /* I believe this is the value at which the timer interrupt will occur */
  timer_set_oc_value(TIM1, TIM_OC1, 24000);

  /**
   * Prescaler divides the clock counter
   * Not sure what we will eventually need it at
   */
  timer_set_prescaler(TIM1, 480 - 1);

  /* So we will eventually want this to be 125ms */
  timer_set_period(TIM1, 48000 - 1);

  /* Enable Timer to generate interrupt signal */
  timer_generate_event(TIM1, TIM_EGR_CC1G | TIM_EGR_TG);
  nvic_enable_irq(NVIC_TIM1_CC_IRQ);
  timer_enable_irq(TIM1, TIM_DIER_CC1IE);

  /* Start the timer */
  TIM_CR1(TIM1) |= TIM_CR1_CEN;
}

void timer2_setup(void)
{

  /* Disable/Reset Timer? */
  timer_set_mode(TIM2, TIM_CR1_CKD_CK_INT, TIM_CR1_CMS_EDGE, TIM_CR1_DIR_UP); // FIXME THIS IS NEW

  /* Enables TIM1_CH1 in Output Compare Mode */
  timer_set_oc_mode(TIM2, TIM_OC1, TIM_OCM_TOGGLE);
  timer_enable_oc_output(TIM2, TIM_OC1);
  timer_enable_break_main_output(TIM2);

  /* I believe this is the value at which the timer interrupt will occur */
  timer_set_oc_value(TIM2, TIM_OC1, 24000);

  /**
   * Prescaler divides the clock counter
   * Not sure what we will eventually need it at
   */
  timer_set_prescaler(TIM2, 480 - 1);

  /* So we will eventually want this to be 125ms */
  timer_set_period(TIM2, 48000 - 1);

  /* Enable Timer to generate interrupt signal */
  timer_generate_event(TIM2, TIM_EGR_CC1G | TIM_EGR_TG);
  nvic_enable_irq(NVIC_TIM2_IRQ);
  timer_enable_irq(TIM2, TIM_DIER_CC1IE);

  /* Start the timer */
  TIM_CR1(TIM2) |= TIM_CR1_CEN;
}