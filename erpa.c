#ifndef STM32F0 // helps Xcode resolve headers
#define STM32F0
#endif

#include <libopencm3/stm32/rcc.h>
#include <libopencm3/stm32/gpio.h>
#include <libopencm3/stm32/timer.h>
#include <libopencm3/stm32/usart.h>
#include <libopencm3/cm3/nvic.h>

#include "samp.h"
#include "uart.h"
#include "HK.h"

static void clock_setup(void)
{
    /* Enable clock at 48mhz */
    rcc_clock_setup_in_hsi_out_48mhz();

    /* Enables clocks for all needed peripherals */
    rcc_periph_clock_enable(GPIO_CLOCKS);
    rcc_periph_clock_enable(SPI1_CLOCK);
    rcc_periph_clock_enable(USART1_CLOCK);
    rcc_periph_clock_enable(TIMER1_CLOCK);

    /* Not using these just yet */
    rcc_periph_clock_enable(IADC_CLOCK);
    rcc_periph_clock_enable(DAC_CLOCK);
}

static void gpio_setup(void)
{
    /** LED GPIO SETUP **/
    /* Set GPIO8 (in GPIO port C) to 'output push-pull'. */
    gpio_mode_setup(LED_PORT, GPIO_MODE_OUTPUT, GPIO_PUPD_NONE, BLUE_LED_PIN | GREEN_LED_PIN);

    /** SPI GPIO SETUP **/
    /* Configure CNV1 GPIO as PA8 (TIM1_CH1) */
    gpio_mode_setup(SPI1_CNV_PORT, GPIO_MODE_AF, GPIO_PUPD_NONE, SPI1_CNV_PIN);
    gpio_set_af(SPI1_CNV_PORT, GPIO_AF2, SPI1_CNV_PIN);
    gpio_set_output_options(SPI1_CNV_PORT, GPIO_OTYPE_PP, GPIO_OSPEED_50MHZ, SPI1_CNV_PIN);

    /* Configure CNV2 GPIO as PB11 (TIM2_CH4) */
    gpio_mode_setup(SPI2_CNV_PORT, GPIO_MODE_AF, GPIO_PUPD_NONE, SPI2_CNV_PIN);
    gpio_set_af(SPI2_CNV_PORT, GPIO_AF2, SPI2_CNV_PIN);
    gpio_set_output_options(SPI2_CNV_PORT, GPIO_OTYPE_PP, GPIO_OSPEED_50MHZ, SPI2_CNV_PIN);

    /////////////////////////////////////////////////////////////////////

    /* Configure SCK1 GPIO as PA5 */
    gpio_mode_setup(SPI1_SCK_PORT, GPIO_MODE_AF, GPIO_PUPD_NONE, SPI1_SCK_PIN);
    gpio_set_af(SPI1_SCK_PORT, GPIO_AF0, SPI1_SCK_PIN);
    gpio_set_output_options(SPI1_SCK_PORT, GPIO_OTYPE_PP, GPIO_OSPEED_100MHZ, SPI1_SCK_PIN);

    /* Configure SCK2 GPIO as PB13 */
    gpio_mode_setup(SPI2_SCK_PORT, GPIO_MODE_AF, GPIO_PUPD_NONE, SPI2_SCK_PIN);
    gpio_set_af(SPI2_SCK_PORT, GPIO_AF0, SPI2_SCK_PIN);
    gpio_set_output_options(SPI2_SCK_PORT, GPIO_OTYPE_PP, GPIO_OSPEED_100MHZ, SPI2_SCK_PIN);

    /////////////////////////////////////////////////////////////////////

    /* Configure MISO GPIO as PA6 */
    gpio_mode_setup(SPI1_MISO_PORT, GPIO_MODE_AF, GPIO_PUPD_NONE, SPI1_MISO_PIN);
    gpio_set_af(SPI1_MISO_PORT, GPIO_AF0, SPI1_MISO_PIN);
    gpio_set_output_options(SPI1_MISO_PORT, GPIO_OTYPE_PP, GPIO_OSPEED_100MHZ, SPI1_MISO_PIN);

    /* Configure MISO GPIO as PB14 */
    gpio_mode_setup(SPI2_MISO_PORT, GPIO_MODE_AF, GPIO_PUPD_NONE, SPI2_MISO_PIN);
    gpio_set_af(SPI2_MISO_PORT, GPIO_AF0, SPI2_MISO_PIN);
    gpio_set_output_options(SPI2_MISO_PORT, GPIO_OTYPE_PP, GPIO_OSPEED_100MHZ, SPI2_MISO_PIN);

    /////////////////////////////////////////////////////////////////////

    /* Setup GPIO pin GPIO_USART1_TX/GPIO9 on GPIO port A for transmit. */
    gpio_mode_setup(USART_TX_GPIO_PORT, GPIO_MODE_AF, GPIO_PUPD_NONE, USART_TX_GPIO_PIN);
    gpio_set_af(USART_TX_GPIO_PORT, GPIO_AF1, USART_TX_GPIO_PIN);
}

int main(void)
{
    clock_setup();
    gpio_setup();
    setupUART();
    spi1_setup();
    spi2_setup();
    timer1_setup();
    timer2_setup();
    while (1)
        ;
}