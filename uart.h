/**
 * @file uart.h
 * @author Shane Woods & Jared King
 * @brief
 * Header files
 * @version 0.1
 * @date 2022-06-30
 */

void setupUART(void);
void putch(int ch);     // Write 8-bit value to UART
void putwd(long wd);    // Write 16-bit value to UART
void putswap(long wd);  // Write 16-bit byte swappped to UART
void printRaw(int raw); // void printRaw(int raw)