
/** @defgroup RCC Clock handles **/
#define GPIO_CLOCKS RCC_GPIOA | RCC_GPIOB | RCC_GPIOC /* clocks for GPIO ports A & B */
#define USART1_CLOCK RCC_USART1                       /* clock for USART1 */
#define SPI1_CLOCK RCC_SPI1                           /* clock for SPI1 interface to 16-bit external ADC (AD7980) */
#define TIMER1_CLOCK RCC_TIM1                         /* CNV clock pulse to external ADC (AD7980) */
#define IADC_CLOCK RCC_ADC                            /* clocks for internal Housekeeping ADCs */
#define DAC_CLOCK RCC_DAC                             /* clocks for DACs (I beleive this is for sweeping, haven't used yet) */

/** @defgroup SPI Handles **/
#define SPI_BUADRATE_PRESCALER SPI_CR1_BAUDRATE_FPCLK_DIV_4
#define SPI_CLOCK_POLARITY SPI_CR1_CPOL_CLK_TO_0_WHEN_IDLE
#define SPI_CPHA_CLOCK_TRANSITION SPI_CR1_CPHA_CLK_TRANSITION_1
#define SPI_MSB_FIRST SPI_CR1_MSBFIRST

/** @defgroup USART Handles **/
#define USART_TX_GPIO_PORT GPIOA
#define USART_TX_GPIO_PIN GPIO9
#define TX_MODE USART_MODE_TX

/** @defgroup GPIO Handles for on STM LEDs **/
#define LED_PORT GPIOC
#define BLUE_LED_PIN GPIO8
#define GREEN_LED_PIN GPIO9

/** @defgroup GPIO Handles for SPI **/
#define SPI1_SCK_PORT GPIOA
#define SPI1_SCK_PIN GPIO5

#define SPI2_SCK_PORT GPIOB
#define SPI2_SCK_PIN GPIO13

#define SPI1_MISO_PORT GPIOA
#define SPI1_MISO_PIN GPIO6

#define SPI2_MISO_PORT GPIOB
#define SPI2_MISO_PIN GPIO14

#define SPI1_CNV_PORT GPIOA
#define SPI1_CNV_PIN GPIO8

#define SPI2_CNV_PORT GPIOB
#define SPI2_CNV_PIN GPIO11

void timer1_setup(void);
void timer2_setup(void);
void spi1_setup(void);
void spi2_setup(void);
